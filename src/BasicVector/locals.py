from .vec2 import Vec2
from .vec3 import Vec3
from .vec4 import Vec4

ORIGIN_VEC2 = Vec2(0, 0)
ORIGIN_VEC3 = Vec3(0, 0, 0)
ORIGIN_VEC4 = Vec4(0, 0, 0, 0)
