import unittest


class MyTestCase(unittest.TestCase):
    def test_importVec2(self):
        import sys
        try:
            from src.BasicVector import Vec2
        except ImportError:
            raise ImportError

        self.assertEqual("src.BasicVector.vec2" in sys.modules, True, "Vec2 was not imported correctly")

    def test_importVec3(self):
        import sys
        try:
            from src.BasicVector import Vec3
        except ImportError:
            raise ImportError

        self.assertEqual("src.BasicVector.vec3" in sys.modules, True, "Vec3 was not imported correctly")

    def test_importVec4(self):
        import sys
        try:
            from src.BasicVector import Vec3
        except ImportError:
            raise ImportError

        self.assertEqual("src.BasicVector.vec4" in sys.modules, True, "Vec4 was not imported correctly")

    def test_importLocals(self):
        import sys
        try:
            from src.BasicVector import Vec3
        except ImportError:
            raise ImportError

        self.assertEqual("src.BasicVector.locals" in sys.modules, True, "Locals were not imported correctly")

    def test_importAll(self):
        import sys
        try:
            import src.BasicVector
        except ImportError:
            raise ImportError

        self.assertEqual("src.BasicVector" in sys.modules, True, "Nothing was imported correctly")


if __name__ == '__main__':
    unittest.main()
